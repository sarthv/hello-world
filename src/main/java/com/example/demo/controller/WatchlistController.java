package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entites.Watchlist;
import com.example.demo.service.WatchlistService;

@RestController
@RequestMapping("/watchlist")
public class WatchlistController {

	@Autowired
	WatchlistService service;

	@GetMapping("/")
	public List<Watchlist> getAll() {
		return service.findAll();
	}

	@GetMapping("/test")
	public String test() {
		return "working";
	}

}
