package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entites.Fundamentals;
import com.example.demo.service.ExchangeService;

@RestController
@RequestMapping("/fundamentals")
public class ExchangeController {

	@Autowired
	private ExchangeService service;

	@GetMapping("/")
	public List<Fundamentals> getAll() {
		return service.getAll();
	}

	@GetMapping("/{ticker}")
	public Fundamentals read(@PathVariable("ticker") String ticker) {
		System.out.println("listening");
		return service.getInfo(ticker);
	}

	@GetMapping("/test")
	public String t() {
		return "test";
	}

//	// Convert an exception to an HTTP status code
//	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "No element with given id")
//	@ExceptionHandler(NoSuchElementException.class)
//	public void noSuchElement() {
//		// The job is done with annotations only :)
//	}

}