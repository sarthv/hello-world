package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entites.Fundamentals;

public interface ExchangeRepository extends JpaRepository<Fundamentals, Integer> {
	public Fundamentals findByTicker(String ticker);
}
