package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entites.Watchlist;

public interface WatchlistRepository extends JpaRepository<Watchlist, Integer> {
}
