package com.example.demo.entites;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fundamentals")
public class Fundamentals implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	private String ticker;
	private String info;

	@OneToOne(mappedBy = "fundamentals", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Watchlist watchlist;

	public Fundamentals() {

	}

	public Fundamentals(Integer id, String ticker, String info) {
		this.id = id;
		this.ticker = ticker;
		this.info = info;
		// this.watchlist = watchlist;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

//	public Watchlist getWatchlist() {
//		return watchlist;
//	}
//
//	public void setWatchlist(Watchlist watchlist) {
//		this.watchlist = watchlist;
//	}
//	// equals & hascode - equality based on business logic not a memory address
//	@Override
//	public boolean equals(Object o) {
//		if (this == o)
//			return true;
//		if (o == null || getClass() != o.getClass())
//			return false;
//		Book book = (Book) o;
//		return Objects.equals(id, book.id) && Objects.equals(author, book.author) && Objects.equals(title, book.title);
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(id, author, title);
//	}
}
