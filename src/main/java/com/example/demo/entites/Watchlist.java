package com.example.demo.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "watchlist")
public class Watchlist implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	// @JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "fundamentals_ticker", nullable = false)
	private Fundamentals fundamentals;

	public Watchlist() {

	}

	public Watchlist(Integer id) {
		this.id = id;
		// this.ticker = ticker;
		// this.fundamentals = fundamentals;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	public String getTicker() {
//		return ticker;
//	}
//
//	public void setTicker(String ticker) {
//		this.ticker = ticker;
//	}

	public Fundamentals getFundamentals() {
		return fundamentals;
	}

	public void setFundamentals(Fundamentals fundamentals) {
		this.fundamentals = fundamentals;
	}
}
