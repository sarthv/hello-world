package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entites.Watchlist;
import com.example.demo.repository.WatchlistRepository;

@Transactional
@Service
public class WatchlistService {

	@Autowired
	private WatchlistRepository repo;

	public List<Watchlist> findAll() {
		return repo.findAll();
	}

}
