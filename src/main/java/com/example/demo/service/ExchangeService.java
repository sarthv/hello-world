package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entites.Fundamentals;
import com.example.demo.repository.ExchangeRepository;

@Transactional
@Service
public class ExchangeService {

	@Autowired
	private ExchangeRepository repository;

	public Fundamentals getInfo(String ticker) {
		return repository.findByTicker(ticker);
	}

	public List<Fundamentals> getAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
